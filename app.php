<?php

	require_once('libraries/core/HttpServer.class.php');

	use \ServerCore\HttpServer;

	set_time_limit(0);

	$ip_address = "0.0.0.0";
	$port_number = 80;

	$hs = new HttpServer($ip_address, $port_number, dirname(__FILE__));
	$hs->create();
	$hs->loop(array(function($uri, $handler){
			printf("FIRST HANDLER\n");
			if(is_file($uri))
				return $handler->file();
		}, function($uri, $handler){
			printf("SECOND HANDLER\n");
			return $handler->file("\config\dispatcher.php");
		})
	);

	$hs->close();