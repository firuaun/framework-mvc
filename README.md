# Framework MVC in PHP

~Praca dyplomowa (magisterska)~

## Dependancies
* PHP >= 5.5.15

## Prerequirements

* turn on rewite engine:
    * in `/etc/apache2/apache2.conf` change:
        ```
        <Directory /var/www>
            AllowOverride None
        </Directory>
        ```
      to:
        ```
        <Directory /var/www>
            AllowOverride All
        </Directory>
        ```
    * execute commands:
        ```
        sudo a2enmod rewrite
        sudo service apache2 restart
        ```
      to run RewriteEngine

* to enable PDO for sqlite:
    * install sqlite3 and extension for php:
        ```
        sudo apt-get install sqlite3
        sudo apt-get install php5-sqlite
        ```
    * add rule in `php.ini`:
        ```
        [PDO_SQLITE]
        extension=pdo_sqlite.so
        ```

## Instalation steps

* substitute directory name for your project root directory towards in the following files:
    * `.htaccess`
    * `config/header.php`