<?php
/**
 * User: pharo
 * Date: 2014-12-22
 * Time: 15:45
 */
include_once './header.php';
include './routes.php';

use libraries\Url;
use libraries\Route;

$requestedURL = new Url($_SERVER["REQUEST_URI"]);

try {
	Route::follow($requestedURL);
}
catch(Exception $ex) {
    //or View::show((new View('responses/internal',array('title'=>'darmobusy - Wewnętrzny błąd','message'=>$ex->getMessage()),'layout')));
    Route::incantate("\controllers\ErrorController::notFound", array("msg"=>$ex->getMessage()));
}
