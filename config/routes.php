<?php
/**
 * User: pharo
 * Date: 2014-12-22
 * Time: 16:29
 */

global $routes;

/*
 * Sadly the route handle has to be a string.
 * See http://php.net/manual/en/language.types.callable.php
 */

$routes = array_reverse(array(
    '/' => '\controllers\HomeController::main'
));
