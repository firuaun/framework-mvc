<?php
/**
 * User: pharo
 * Date: 2014-12-22
 * Time: 16:26
 */
define("ROOT_HOST_PATH",'/mvc/');
define("DEFAULT_ROOT_PATH",'/');
define("ROOT_REAL_PATH", dirname(__DIR__));
define("DIR_SEP", DIRECTORY_SEPARATOR);
define("TMP_DIR_PATH", ROOT_REAL_PATH . DIRECTORY_SEPARATOR . "tmp");


define("DATABASE_TYPE", "sqlite");
define("DATABASE_HOST", TMP_DIR_PATH . DIR_SEP . "memory.sqlite");

function strip_namespace($class_name) {
    return str_replace("\\", DIR_SEP, $class_name);
}

function autoLoader($class_name){
    $file = sprintf("%s.class.php",
        ROOT_REAL_PATH . DIR_SEP . strip_namespace($class_name));
    if(file_exists($file)){
        require_once $file;
    }
}

spl_autoload_register('autoLoader');