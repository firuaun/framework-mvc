<?php

namespace test\environment;

use test\framework\TestTraits;
use libraries\Environment;
use libraries\Model;
use libraries\Database;

class TestEnvironment extends Environment implements TestTraits {

    public function __construct($db_path) {
        $this->db_path = $db_path;
    }

    public function SetUp() {
        Model::DeinitializeDatabase();
        Model::SetEnvironment($this);
    }

    public function TearDown() {
        Model::DeinitializeDatabase();
        Database::removeCustomInstance($this->getDatabasePath());
    }

    public function getDatabasePath() {
        return sprintf("%s%s%s",
            $this->getTemporatyPath(),
            DIR_SEP,
            $this->db_path);
    }

    public function TestSetSetUp() {}
    public function TestSetTearDown() {}

    private $db_path;
}