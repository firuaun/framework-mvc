<?php

namespace test\libraries\models;

use libraries\Model;
use libraries\model\Collection;

class Flag extends Model {

    static public $mapping;

}

Flag::$mapping = array(
    "name" => Model::TEXT,
    "users" => Collection::Of(User::class)->Via('flags')
);