<?php

namespace test\libraries\models;

use libraries\Model;
use libraries\model\Single;

class Email extends Model {

	public static $mapping;

}

Email::$mapping = array(
	"address" 	=> Model::TEXT,
	"user"		=> Model::Of(User::class)
);