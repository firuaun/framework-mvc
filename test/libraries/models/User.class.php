<?php

namespace test\libraries\models;

use libraries\Model;
use libraries\model\Collection;

class User extends Model {
	public static $mapping;
}

User::$mapping = array(
	"name" 	=> Model::TEXT,
	"mails" => Collection::Of(Email::class)->Via("user"),
	"indeks" => Model::NUMBER,
	"flags" => Collection::Of(Flag::class)->Via('users')
);