<?php

namespace test\libraries;

use test\framework\Fixture;
use test\framework\Assert;
use test\environment\TestEnvironment;

use test\libraries\models\User;
use test\libraries\models\Email;
use test\libraries\models\Flag;

class DynamicModelTest extends Fixture {

    public function __construct() {
        $this->environment = new TestEnvironment('ModelTest.db');
    }

    public function SetUp() {
        $this->environment->SetUp();
        User::instantiate();
        Email::instantiate();
        Flag::instantiate();
    }

    public function TearDown() {
        $this->environment->TearDown();
    }

    public function test_WhenNewModel_FindAfterSave() {
        $u = new User(array("name" => "pharo", "indeks" => 1176729));

        $us = User::find();
        Assert::Equal(0, count($us), "There should be no users.");

        $u->save();

        $us = User::find();
        Assert::Equal(1, count($us), "There should be exactly 1 user.");
    }

    public function test_WhenCreateModel_SaveWhileCreated() {
        $u = User::create(array("name" => "pharo", "indeks" => 1176729));

        $us = User::find();
        Assert::Equal(1, count($us), "There should be exactly 1 user.");
    }

    public function test_WhenModelPrimitiveMemberChanged_SaveExplicitly() {
        $u = User::create(array("name" => "pharo", "indeks" => 1176729));

        $u->name = "firuaun";

        $us = User::find(array("id"=>$u->Id()))[0];
        Assert::Equal("pharo", $us->name, "There should be unsaved name in database.");

        $u->save();

        $us = User::find(array("id"=>$u->Id()))[0];
        Assert::Equal("firuaun", $us->name, "There should be new name in database.");
    }

    public function test_WhenModelRelationalMappedMemberChanged_SaveImplicitImmediately() {
        $u = User::create(array("name" => "pharo", "indeks" => 1176729));

        $u->mails[] = new Email(array("address"=>"pharo@op.pl"));

        $us = User::find(array("id"=>$u->Id()))[0];
        Assert::Equal("pharo@op.pl", $us->mails[0]->address, "There should be an emial in database already saved.");
    }

    public function test_WhenModelsManyToManyRelation_ValidConnectionsAreMade() {
        $pharo = User::create(array("name" => "pharo", "indeks" => 1176729));
        $firuaun = User::create(array("name" => "firuaun", "indeks" => 201529));

        $student = Flag::create(array("name"=>"student"));
        $employee = Flag::create(array("name"=>"employee"));
        $teacher = Flag::create(array("name"=>"teacher"));
        $reasercher = Flag::create(array("name"=>"reasercher"));

        $pharo->flags = array($student, $employee);
        $firuaun->flags = array($teacher, $reasercher);

        Assert::Equal(4, count(Flag::find()), "There should be 4 flags.");
        Assert::Equal(2, count(User::find()), "There should be 2 users.");

        Assert::Equal(2, count(User::find(array("name"=>"pharo"))[0]->flags), "There should be 2 flags for pharo.");
        Assert::Equal(2,  count(User::find(array("name"=>"firuaun"))[0]->flags), "There should be 2 flags for firuaun.");

        $fes = Flag::find(array("name"=>"employee"))[0];
        $fes->users[] = $firuaun;

        Assert::Equal(4, count(Flag::find()), "There should be still 4 flags.");
        Assert::Equal(2, count(User::find()), "There should be still 2 users.");

        Assert::Equal(2, count(User::find(array("name"=>"pharo"))[0]->flags), "There should be still 2 flags for pharo.");
        Assert::Equal(3,  count(User::find(array("name"=>"firuaun"))[0]->flags), "There should be 3 flags now for firuaun.");
    }

}