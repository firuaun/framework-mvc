<?php

namespace test\libraries;

use test\framework\Fixture;
use test\framework\Assert;
use test\environment\TestEnvironment;

use test\libraries\models\User;
use test\libraries\models\Email;
use test\libraries\models\Flag;

class StaticModelTest extends Fixture {

    public function __construct() {
        $this->environment = new TestEnvironment('ModelTest.db');
    }

    private function prepareData() {
        User::instantiate();
        Email::instantiate();
        Flag::instantiate();
    	$u = User::create(array(
    			"name" => "pharo",
    			"indeks" => 1176729
    		));
    	$e = Email::create(array("address"=>"pharo@op.pl"));
        $u->mails = array($e);
        $e2 = new Email();
        $e2->address = "firuaun@gmail.com";
        $u->mails[] = $e2;

        $f = Flag::create(array("name" => "administrator"));
        $u->flags = array($f);
        $of = new Flag();
        $of->name = "user";
        $u->flags[] = $of;
    }

    public function TestSetSetUp() {
        $this->environment->SetUp();
        $this->prepareData();
    }

    public function TestSetTearDown() {
        $this->environment->TearDown();
    }

    public function test_WhenFindUserWithoutParams_ThenGetAll() {
        $us = User::find();
        Assert::Equal(1, count($us), "There should be exactly 1 user.");
    }

    public function test_WhenFindUserWithId_ThenGotValid() {
        $u = User::findById(1);
        Assert::Equal("pharo", $u->name, "The user name doesn't match.");
    }

    public function test_WhenFindUserEmails_ThenGetAll() {
        $u = User::findById(1);
        Assert::Equal(2, count($u->mails), "There should be two mails for the user.");
    }

    public function test_WhenAskForUserEmails_ThenGetAllAddresses() {
        $u = User::findById(1);
        Assert::ArrayEqual(
            array("pharo@op.pl", "firuaun@gmail.com"),
            array_map(function($email){ return $email->address; }, $u->mails->toArray()),
            "There should be two mails for the user.");
    }

    public function test_WhenFindEmails_ThenGetAll() {
        $es = Email::find();
        Assert::Equal(2, count($es), "There should be exactly 2 emails.");
    }

    public function test_WhenFindEmailsWithId_ThenGetValid() {
        $e = Email::findById(1);
        Assert::InArray($e->address, array("pharo@op.pl", "firuaun@gmail.com"),
            "There is no such address.");
    }

    public function test_WhenAskForUserAndNameInEmail_ThenNamesMatch() {
        $e = Email::findById(1);
        $u = User::findById(1);
        Assert::Equal($u->name, $e->user->name, "This is not the same user");
    }

    public function test_WhenAskUserFlags_ThenGetAll() {
        $u = User::findById(1);
        Assert::Equal(2, count($u->flags), "The number of user flags doesn't match");
    }

    public function test_WhenAskForUserFlag_ThenGetAllFlagNames() {
        $u = User::findById(1);
        Assert::ArrayEqual(
            array("administrator", "user"),
            array_map(function($flag){ return $flag->name; }, $u->flags->toArray()),
            "There is an error with the flag names.");
    }

    public function test_WhenFindFlags_ThenGetAll() {
        $fs = Flag::find();
        Assert::Equal(2, count($fs), "There should be exactly 2 flags.");
    }

    public function test_WhenFindFlagWithId_ThenGetValid() {
        $f = Flag::findById(1);
        Assert::InArray($f->name, array("administrator", "user"),
            "There is no such flag.");
    }


}