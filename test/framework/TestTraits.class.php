<?php

namespace test\framework;

interface TestTraits {
    public function SetUp();
    public function TearDown();

    public function TestSetSetUp();
    public function TestSetTearDown();
}