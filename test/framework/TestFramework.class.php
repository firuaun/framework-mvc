<?php

namespace test\framework;

class TestFramework {

    public static function RegisterTestSets() {

        foreach (func_get_args() as $test) {
            echo "Running test: ".$test."\n";
            $test::Run();
        }

    }

}