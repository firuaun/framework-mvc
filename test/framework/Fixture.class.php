<?php

namespace test\framework;

abstract class Fixture implements TestTraits {

    const TEST_FAIL_RESULT = 0;
    const TEST_OK_RESULT = 1;

    public function __construct() {}

    public static function Run() {

        $test_name = get_called_class();
        $test_obj = new $test_name();

        assert_options(ASSERT_CALLBACK, array($test_obj, 'assertCallback'));

        $test_obj->TestSetSetUp();

        foreach(get_class_methods($test_name) as $method) {

            if (strpos($method, 'test') === 0) {
                $test_obj->SetUp();
                echo sprintf("[RUN   ] %s\n", $method);

                $test_obj->testSetTable[$method] = Fixture::TEST_OK_RESULT;
                $test_obj->currentTest = $method;
                try
                {
                    $test_obj->$method();
                }
                catch(Assert $ex) {
                    $test_obj->testSetTable[$method] = Fixture::TEST_FAIL_RESULT;
                    echo $ex->Message();
                }

                echo sprintf("[%s] %s\n", $test_obj->testSetTable[$method] ? "    OK" : "  FAIL", $method);

                $test_obj->TearDown();
            }
        }

        $test_obj->TestSetTearDown();
        self::Summary($test_obj);

    }

    public static function Summary($test) {
        $set = $test->GetTestSetResult();

        $setValStat = array_count_values(array_values($set));

        $total = count($set);
        $passed = array_key_exists(Fixture::TEST_OK_RESULT, $setValStat) ? $setValStat[Fixture::TEST_OK_RESULT] : 0;
        $failed = $total - $passed;

        echo sprintf("Test passed %d of %d (%d%%, %d failed).\n",
            $passed,
            $total,
            $passed/$total * 100,
            $failed);
    }

    public function assertCallback($file, $line, $code) {
        echo sprintf("Failed %s:%d:%s\n", $file, $line, $code);
        $this->testSetTable[$this->currentTest] = Fixture::TEST_FAIL_RESULT;
    }

    public function SetUp() {}
    public function TearDown() {}

    public function TestSetSetUp() {}
    public function TestSetTearDown() {}

    public function GetTestSetResult() {
        return $this->testSetTable;
    }

    private $testSetTable;
    private $currentTest;
};
