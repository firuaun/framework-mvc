<?php

namespace test\framework;

use Exception;

function array_both_diff($array1, $array2) {
    $intersect = array_intersect($array1, $array2);
    return array_merge(array_diff($array1, $intersect), array_diff($array2, $intersect));
}

function array_values_equal($array1, $array2) {
    return empty(array_both_diff($array1, $array2));
}

class Assert extends Exception {

    public function __construct($expected, $get, $desc, $tobe) {
        $this->expect = $expected;
        $this->got = $get;
        $this->desc = $desc;
        $this->tobe = $tobe;
    }

    public static function Equal($a, $b, $desc = "") {
        if ($a === $b) {
            return;
        }
        throw new Assert($a, $b, $desc, "to be equal");
    }

    public static function NotEqual($a, $b, $desc = "") {
        if ($a !== $b) {
            return;
        }
        throw new Assert($a, $b, $desc, "to be not equal");
    }

    public static function ArrayEqual($a, $b, $desc = "") {
        if (array_values_equal($a, $b)) {
            return;
        }
        throw new Assert(json_encode($a), json_encode($b), $desc, "to be equal");
    }

    public static function InArray($value, $arr, $desc = "") {
        if (in_array($value, $arr)) {
            return;
        }
        throw new Assert(json_encode($arr), $value, $desc, "to be in array");
    }

    public function Message() {
        return sprintf(
            "Asserted values %s.\nExpected: %s\nGot: %s\nAsserted that: \"%s\".\n",
            $this->tobe,
            $this->expect,
            $this->got,
            $this->desc
        );
    }

    private $expect;
    private $got;
    private $desc;
    private $tobe;
}