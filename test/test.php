<?php

//Runs from root directory
require_once("config/header.php");

use test\framework\TestFramework;

TestFramework::RegisterTestSets(
    test\libraries\StaticModelTest::class,
    test\libraries\DynamicModelTest::class
);