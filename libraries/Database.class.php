<?php
/**
 * User: pharo
 * Date: 2014-12-23
 * Time: 16:48
 */

namespace libraries;

use PDO;

class Database {
    private static $db;

    static function getInstance(){
        if(is_null(Database::$db)){
            Database::$db = self::getCustomInstance(DATABASE_TYPE, DATABASE_HOST);
        }
        return Database::$db;
    }

    static function removeDatabase(){
        self::removeCustomInstance(DATABASE_HOST);
    }

    public static function getCustomInstance($protocol, $path) {
        return new PDO(sprintf("%s:%s", $protocol, $path));
    }

    public static function removeCustomInstance($path) {
        if(file_exists($path)){
            unlink($path);
        }
    }

    public function getInstanceForEnvironment(Environment $env) {
        return self::getCustomInstance($env->getDatabaseProtocol(),
                                       $env->getDatabasePath());
    }

}