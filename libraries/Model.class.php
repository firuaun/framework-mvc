<?php
/**
 * User: pharo
 * Date: 2014-12-23
 * Time: 16:47
 */

namespace libraries;

use PDO;
use libraries\model\RelationMappingInterface;
use libraries\model\Collection;
use libraries\model\Single;

abstract class Model {

    static private $db;
    static private $environment;

    public $attributes;
    static public $mapping;

    protected $isPersisted = false;
    protected $isRdy       = false;

    const NUMBER        = "INTEGER";
    const TEXT          = "TEXT";
    const BLOB          = "BLOB";
    const PRIMARY_KEY   = "INTEGER PRIMARY KEY";

    public function __construct($initial = null) {
        if(!is_null($initial)) {
            $this->attributes = $initial;
        }
        if(isset($this->attributes["id"])) {
            $this->isPersisted = true;
        }
        $this->isRdy = true;
    }

    public static function DeinitializeDatabase() {
        self::$db = null;
    }

    public static function db() {
        if (self::$db == null) {
            self::$db = Database::getInstanceForEnvironment(Model::$environment);
        }
        return self::$db;
    }

    public static function findById($id){
        $class = get_called_class();
        return $class::find(array("id"=>$id))[0];
    }

    public static function strip_namespace($class) {
        return end(explode('\\', $class));
    }

    public static function find(array $conditions = null, $class = null) {
        $class = is_null($class) ? get_called_class() : $class;
        $statement = QueryUtils::prepare_select_statement(Model::strip_namespace($class), $conditions);
        return self::db()->query($statement)->fetchAll(PDO::FETCH_CLASS, $class);
    }

    public function Id() {
        return $this->attributes["id"];
    }

    public static function create(array $config) {
        $class = get_called_class();
        $object = new $class($config);
        return $object->save();
    }

    public function save() {
        return $this->isPersisted ? $this->update() : $this->insert();
    }
    public function delete($id) {
        //TODO: definitevely do this
        throw new Exception("delete method under construction", 1);
    }

    public function insert() {
        $query = self::db()->prepare(QueryUtils::prepare_insert_statement(Model::strip_namespace(get_class($this)), self::table_attr_keys($this->attributes)));
        $insertResult = $query->execute(self::table_attr_values($this->attributes));
        if($insertResult) {
            $this->id = self::db()->lastInsertId();
            $this->isPersisted = true;
            return $this;
        }
        return $insertResult;
    }

    private static function table_attr_keys($attr) {
        $keys = array();
        foreach($attr as $key => $value) {
            if(!is_array($value) && !is_object($value)) {
                $keys[] = $key;
            }
        }
        return $keys;
    }

    private static function table_attr_values($attr) {
        $values = array();
        foreach($attr as $key => $value) {
            if(!is_array($value) && !is_object($value)) {
                $values[] = $value;
            }
        }
        return $values;
    }

    public function update() {
        $q = QueryUtils::prepare_update_statement(Model::strip_namespace(get_class($this)), self::table_attr_keys($this->attributes),sprintf("id=%d",$this->id));
        $query = self::db()->prepare($q);
        $updateResult = $query->execute(self::table_attr_values($this->attributes));
        if($updateResult) {
            return $this;
        }
        return $updateResult;
    }

    public function __set($name,$value) {
        $class = get_called_class();
        if ($this->isRdy && isset($class::$mapping[$name]) && $class::$mapping[$name] instanceof RelationMappingInterface) {
            $this->populate($name, $value);
        }
        else
            $this->attributes[$name] = $value;
    }

    public function &__get($name) {
        $class = get_called_class();
        if (isset($class::$mapping[$name]) &&
            $class::$mapping[$name] instanceof RelationMappingInterface &&
            (!isset($this->attributes[$name]) || is_numeric($this->attributes[$name]))) {
                return $this->populate($name);
        }
        else
            return $this->attributes[$name];
    }

    public function &populate($name, $value = null) {
        $model = get_called_class();
        return $model::$mapping[$name]->Populate($this, $name, $value);
    }

    private static function prepend_id_attr(&$attr) {
        $attr["id"] = Model::PRIMARY_KEY;
        return $attr;
    }

    public static function instantiate() {
        $className = get_called_class();
        $strippedClassName = Model::strip_namespace($className);
        $mapping = $className::$mapping;
        $foreign_keys = array();
        foreach($mapping as $key => $value) {
            if($value instanceof RelationMappingInterface) {
                if($value instanceof Collection) { // is Collection <=> Collection mapping
                    $other_model = $value->model_class;
                    $strippedOtherClassName = Model::strip_namespace($other_model);
                    if ($other_model::$mapping[$value->via_name] instanceof Collection) {
                        if($query = QueryUtils::generate_twin_model_rel_table(
                                                    $strippedClassName,
                                                    $strippedOtherClassName)) {
                            self::db()->exec($query);
                        }
                    }
                    unset($mapping[$key]);
                }
                else {
                    $foreign_keys[$key] = Model::strip_namespace($value->model_class);
                    $mapping[$key] = Model::NUMBER;
                }
            }
        }
        $query = QueryUtils::prepare_create_table_statement($strippedClassName, self::prepend_id_attr($mapping), $foreign_keys);
        self::db()->exec($query);
    }

    public static function SetEnvironment(Environment $env) {
        Model::$environment = $env;
    }

    public static function Of($class) {
        return (new Single($class));
    }

}

Model::SetEnvironment(new Environment());