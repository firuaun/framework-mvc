<?php

namespace libraries;

class Environment {

    public function getTemporatyPath() {
        return TMP_DIR_PATH;
    }

    public function getRootHostPath() {
        return ROOT_HOST_PATH;
    }

    public function getDefaultRootPath() {
        return DEFAULT_ROOT_PATH;
    }

    public function getRootRealPath() {
        return ROOT_REAL_PATH;
    }

    public function getDatabasePath() {
        return DATABASE_HOST;
    }

    public function getDatabaseProtocol() {
        return DATABASE_TYPE;
    }

}