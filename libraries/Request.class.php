<?php
/**
 * User: pharo
 * Date: 2014-12-22
 * Time: 17:00
 */

namespace libraries;

class Request {
    private $parameters;

    public function __construct($params){
        $this->parameters = $params;
    }

    public function param($name){
        return $this->parameters[$name];
    }

    public function hasParam($name) {
        return array_key_exists($name,$this->parameters);
    }

    public function getParameters(){
        return $this->parameters;
    }
}