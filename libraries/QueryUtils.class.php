<?php
/**
 * User: pharo
 * Date: 2014-12-23
 * Time: 16:48
 */

namespace libraries;

class QueryUtils {
    const INSERT_STATEMENT = "INSERT INTO %s %s VALUES %s";
    const UPDATE_STATEMENT = "UPDATE %s SET %s WHERE %s";
    const SELECT_STATEMENT = "SELECT * FROM %s %s";
    const WHERE_STATEMENT  = "WHERE %s";

    const CREATE_TABLE_STATEMENT = "CREATE TABLE %s(%s)";
    const CREATE_TABLE_ATTR_STATEMENT = "%s %s";
    const CREATE_TABLE_PRIMATY_KEY_STATEMENT = "%s %s";
    const FOREIGN_KEY_CONSTRAINT_STATEMENT = "FOREIGN KEY(%s) REFERENCES %s(%s)";

    public static function array_key_value_to_string($array,$pair_separator='=',$between_separator=', ', $key_quote = '`', $value_quote = "'"){
        $statement = array();
        foreach($array as $key => $value) {
            $statement[] = sprintf("%s%s%s%s%s%s%s",$key_quote,$key,$key_quote,
                $pair_separator,
                $value_quote,$value,$value_quote);
        }
        return implode($between_separator,$statement);
    }

    public static function array_value_to_string($array,$value_quote="'",$between_separator=', ',$parenthesis="()"){
        $statement = array();
        foreach($array as $value){
            $statement[] = sprintf("%s%s%s",$value_quote,$value,$value_quote);
        }
        return $parenthesis[0].implode($between_separator,$statement).$parenthesis[1];
    }

    public static function prepare_insert_statement($into, $key_set){
        return sprintf(QueryUtils::INSERT_STATEMENT,
            $into,
            QueryUtils::array_value_to_string($key_set,''),
            QueryUtils::array_value_to_string(array_fill(0,count($key_set),'?'),'',', ')
        );
    }

    public static function prepare_update_statement($into, $key_set, $where) {
        return sprintf(QueryUtils::UPDATE_STATEMENT,
            $into,
            QueryUtils::array_key_value_to_string(array_fill_keys($key_set,'?'),'=',', ','',''),
            $where
        );
    }

    public static function prepare_select_statement($from, $where = null) {
        return sprintf(
            self::SELECT_STATEMENT,
            $from,
            is_null($where)
                ? ''
                : sprintf(self::WHERE_STATEMENT, QueryUtils::array_key_value_to_string($where,'=',' AND ', '',"'")));
    }

    public static function prepare_create_table_statement($table_name, $attributes, $foreign_keys) {
        $foreign_keys_items = array();
        foreach($foreign_keys as $key => $value) {
            $foreign_keys_items[] = sprintf(QueryUtils::FOREIGN_KEY_CONSTRAINT_STATEMENT,
                $key, $value, "id");
        }
        return sprintf(QueryUtils::CREATE_TABLE_STATEMENT,
            $table_name,
            implode(', ',
                    array_filter(
                        array(QueryUtils::array_key_value_to_string($attributes, ' ', ', ', '', ''),
                              implode(', ',$foreign_keys_items)),
                        function($value) {return $value !== '';}
                        )));
    }

    private static function check_table_exists($db, $table_name) {
        return $db->query("SELECT COUNT(*) FROM sqlite_master WHERE name = $table_name")->fetch(PDO::FETCH_NUM)[0] > 0;
    }

    public static function generate_twin_model_rel_table($first, $second) {
        if ($first < $second) { // to this only to the one lexically first
            $tab_name = $first."_".$second;

            //if (!self::check_table_exists($tab_name)) {

                $attributes = array(
                    "$first" => Model::NUMBER,
                    "$second" => Model::NUMBER
                );

                $foreign_keys_items = array(
                    "$first" => $first,
                    "$second" => $second
                );

                return self::prepare_create_table_statement($tab_name, $attributes, $foreign_keys_items);
           // }
        }
        else {
            return false;
        }
    }

    const SELECT_NAMESPACED_STETEMENT = "SELECT %s.* FROM %s WHERE %s";
    const JOIN_ON_STATEMENT = "JOIN %s ON %s";

    public static function generate_select_for_twin_rel_table($id, $current_model, $search_for_model) {
        $first = $current_model;
        $second = $search_for_model;
        if ($first > $second) {
            // hottest swap technique
            list($first, $second) = array($second, $first);
        }
        $table_name = $first."_".$second;

        $join_stmts = array();

        $join_on = array($first, $second);

        foreach ($join_on as $value) {
            $join_stmts[] = sprintf(QueryUtils::JOIN_ON_STATEMENT,
                                $value,
                                sprintf("%s.id = %s.%s", $value, $table_name, $value)
                            );
        }

        return sprintf(QueryUtils::SELECT_NAMESPACED_STETEMENT,
            $search_for_model,
            implode(' ', array_merge(array($table_name), $join_stmts)),
            sprintf("%s.id = %s", $current_model, $id)
        );
    }

    const DELETE_STATEMENT = "DELETE FROM %s WHERE %s";

    public static function generate_delete_for_twin_rel_table($id, $current_model, $search_for_model) {
        $first = $current_model;
        $second = $search_for_model;
        if ($first > $second) {
            // hottest swap technique
            list($first, $second) = array($second, $first);
        }
        $table_name = $first."_".$second;

        return sprintf(QueryUtils::DELETE_STATEMENT,
            $table_name,
            sprintf("%s = %s", $current_model, $id)
        );
    }

    public static function generate_insert_for_twin_rel_table($id, $current_model, $insert_model, $values) {
        $first = $current_model;
        $second = $insert_model;
        if ($first > $second) {
            // hottest swap technique
            list($first, $second) = array($second, $first);
        }
        $table_name = $first."_".$second;

        $out_queries = array();

        foreach($values as $value) {
            $out_queries[] = sprintf(
                QueryUtils::INSERT_STATEMENT,
                    $table_name,
                    QueryUtils::array_value_to_string(array($current_model, $insert_model),''),
                    QueryUtils::array_value_to_string(array($id, $value),'')
            );
        }

        return $out_queries;
    }




}