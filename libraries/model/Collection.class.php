<?php

namespace libraries\model;

use ArrayIterator;
use ArrayAccess;
use Countable;
use PDO;
use IteratorAggregate;

use libraries\QueryUtils;
use libraries\Model;

class PopulatableArray implements ArrayAccess, IteratorAggregate, Countable {
    private $data;
    private $object_ref;
    private $name;

    public function __construct($data = array(), $name = "", $ref = null) {
        $this->data = $data;
        $this->object_ref = $ref;
        $this->name = $name;
    }

    public function offsetGet($offset) {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

    public function offsetSet($offset, $value) {
        if ($offset === null) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
        $this->object_ref->populate($this->name, $this->data);
    }

    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->data[$offset]);
        $this->object_ref->populate($this->name, $this->data);
    }

    public function getIterator() {
        return new ArrayIterator($this->data);
    }

    public function count() {
        return count($this->data);
    }

    public function& toArray() {
        return $this->data;
    }
}

class Collection implements RelationMappingInterface {

    public function __construct($model_class) {
        $this->model_class = $model_class;
        $this->via_name = null; //$model_class."_id";
    }

    public function Via($via_name) {
        $this->via_name = $via_name;
        return $this;
    }

    public function &Populate($self, $attr, $value = null) {
        $model = $this->model_class;
        $via = $this->via_name;
        $to_return = $self;
        if(!is_null($value)) { // write
            if (is_array($value)) {
                if ($model::$mapping[$via] instanceof Collection) {

                    $drop_query = QueryUtils::generate_delete_for_twin_rel_table(
                                            $self->Id(),
                                            Model::strip_namespace(get_class($self)),
                                            Model::strip_namespace($model));

                    Model::db()->exec($drop_query);

                    $values = array();
                    foreach($value as $v) {
                        $v->save();
                        $values[] = $v->Id();
                    }

                    // wish this was one query
                    $insert_queries = QueryUtils::generate_insert_for_twin_rel_table(
                                            $self->Id(),
                                            Model::strip_namespace(get_class($self)),
                                            Model::strip_namespace($model),
                                            $values);

                    foreach($insert_queries as $query) {
                        Model::db()->exec($query);
                    }

                }
                else {
                    foreach($value as $row) {
                        $row->attributes[$via] = $self->Id();
                        $row->save();
                    }
                }
            }
            else {
                $value->attributes[$via] = $self->Id();
                $value->save();
            }
            $to_return = $value;
        }
        else {  // read
            if ($model::$mapping[$via] instanceof Collection) {

                $select_query = QueryUtils::generate_select_for_twin_rel_table(
                                        $self->Id(),
                                        Model::strip_namespace(get_class($self)),
                                        Model::strip_namespace($model));

                $results = Model::db()->query($select_query)->fetchAll(PDO::FETCH_CLASS, $model);
            }
            else {
                $results = $model::find(array($via=>$self->Id()));
            }
            $to_return = new PopulatableArray($results, $attr, $self);
            $self->attributes[$attr] = $to_return;
        }
        return $to_return;
    }

    public static function Of($class) {
            return (new Collection($class));
    }
}
