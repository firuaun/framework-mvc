<?php

namespace libraries\model;

class Single implements RelationMappingInterface {

    public function __construct($model_class) {
        $this->model_class = $model_class;
        $this->via_name = null;
    }

    public function Via($via_name) {
        $this->via_name = $via_name;
        return $this;
    }

    public function &Populate($self, $attr, $value = null) {
        $model = $this->model_class;
        $to_return = $self;
        if (!is_null($value)) { // is it about to be saved
            if(is_null($this->via_name)) { // one-to-one
                //TODO: check if via is not set otherwise throw
                $value->attributes[$this->via_name] = $self->Id();
                $value->save();
                $to_return = $value;
            }
            $self->attributes[$attr] = $value->Id();
            $self->save();
        }
        else { // does it need to be fetched
            $other_id = $self->attributes[$attr];
            $to_return = $model::find(array("id"=>$other_id))[0];
            $self->attrbutes[$attr] = $to_return;
        }
        return $to_return;

    }
    public static function Of($class) {
        return (new Single($class));
    }
}