<?php

namespace libraries\model;

interface RelationMappingInterface {

    public function Via($name);

    public function &Populate($self, $attr, $value = null);

}