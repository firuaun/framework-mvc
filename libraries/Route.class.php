<?php
/**
 * User: pharo
 * Date: 2014-12-22
 * Time: 17:31
 */

namespace libraries;

class Route {

    private $request;
    private $response;
    private $handle;

    private function __construct($handle, $params = array()){
        $this->request = new Request($params+$_REQUEST);
        $this->response = new Response();
        $this->handle = $this->process_handle($handle);
    }

    public function invoke(){
        $this->handle->invoke(null,$this->request,$this->response);
    }

    private function process_handle($handle) {
        $handle = explode("::",$handle);
        return new \ReflectionMethod($handle[0],$handle[1]);
    }

    static private function route_as_regex($route) {
        $route = str_replace('/','\/',$route);
        $param_regex = "/\:([^\/\\\]+)/i";
        $param_replace = '(?<${1}>[^\/]+?)';
        return sprintf("/^%s(?:($|\/$))/i",preg_replace($param_regex,$param_replace,$route));
    }

    static private function parse_route(URL $requested_url){
        global $routes;
        foreach($routes as $key => $value){
            $matches = null;
            preg_match(Route::route_as_regex($key),$requested_url->getUrl(),$matches);
            if(count($matches) != 0) {
                return new Route($value,$matches);
            }
        }
        // route not found, so throw an exception
        throw new Exception('Strona o podanym adresie nie istanieje');
    }

    static public function incantate($handle, $params = null) {
        $r = new Route($handle, $params);
        $r->invoke();
        return $r;
    }

    static public function follow(URL $requested_url) {
        $r = self::parse_route($requested_url);
        $r->invoke();
        return $r;
    }
}