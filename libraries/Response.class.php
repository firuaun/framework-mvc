<?php
/**
 * User: pharo
 * Date: 2014-12-22
 * Time: 17:01
 */

namespace libraries;

class Response {
    public function view($viewName, $locales, $layout=null){
        $view = new View($viewName,$locales,$layout);
        echo $view->render();
        return true;
    }
    public function json($obj){
        echo json_encode($obj);
        return true;
    }
    public function send($text){
        echo $text;
        return true;
    }
}