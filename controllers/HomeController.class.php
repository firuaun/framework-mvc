<?php
/**
 * User: pharo
 * Date: 2014-12-22
 * Time: 19:27
 */

namespace controllers;

use libraries\Request;
use libraries\Response;

use models\User;
use models\Email;
use models\Flag;

class HomeController {

    public static function main(Request $req, Response $res) {
        return $res->view(
            'home',
            array(
                'title'=>'Framework MVC - Strona główna'
            ),
            'layout'
        );
    }

}