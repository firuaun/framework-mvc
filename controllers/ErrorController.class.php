<?php
/**
 * User: pharo
 * Date: 2017-01-15
 * Time: 10:23
 */

namespace controllers;

use libraries\Request;
use libraries\Response;

class ErrorController {

    public static function notFound(Request $req, Response $res) {
    	return $res->view('responses/internal',
                        array('title'=>'404 - Nie znaleziono',
                              'message'=>$req->param('msg')),
                        'layout');
    }

}